package Car;

import java.util.ArrayList;
import java.util.List;

public abstract class Car extends Vehicle {

    public int gearNo;
    public int gear;
    public double consumptionPer100Km; //consumption for 15'' tires and in first gear
    public double consumptionIncreasePerGearShift; //consumption for 15'' tires and in first gear


    public double newConsumptionPer100km;

    public Car(int gearNo, int gear, double consumptionPer100Km, double consumptionIncreasePerGearShift) {
        super(consumptionPer100Km);
        this.gearNo = gearNo;
        this.gear = gear;
        this.consumptionPer100Km = consumptionPer100Km;
        this.consumptionIncreasePerGearShift = consumptionIncreasePerGearShift;

    }


    public void shift(int gear) {
        if (gear <= gearNo) {

            if (gear > 1) {
                    System.out.println("shifting to: " + gear);
                    super.consumptionPer100Km = consumption + consumptionIncreasePerGearShift * (gear - 1);
            }
        }
    }
}





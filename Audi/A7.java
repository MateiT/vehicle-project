package Audi;


import Car.Car;

public class A7 extends Car implements IAudi {

    String carColour;
    public String audiCarName; //used to name the phone model
    public String audiChasisNo;
    public int audiGear;
    private String fuelType;

    private int consumptionA7;


    private static int getAudiFuelTankSize() {
        return audiFuelTankSize;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public A7(String carColour, String audiCarName, String audiChasisNo, int audiGear) {
        super(7, 1, 5, 0.25);
        this.carColour = carColour;
        this.audiCarName = audiCarName;
        this.audiChasisNo = audiChasisNo;
        this.audiGear = audiGear;


    }

    @Override
    public void reFillTank(double availableFuel) {
        if (this.availableFuel + availableFuel > audiFuelTankSize + 9) {
            this.availableFuel = audiFuelTankSize + 9;
            System.out.println("can't fill with more than maximum A7 tank capacity");
            System.out.println("tank full");
        } else {
            this.availableFuel = availableFuel + this.availableFuel;
            System.out.println("available fuel in car after refill is: " + this.availableFuel);
        }

    }


    @Override
    public String toString() {
        return "A7{" +
                "carColour='" + carColour + '\'' +
                ", audiCarName='" + audiCarName + '\'' +
                ", audiChasisNo='" + audiChasisNo + '\'' +
                ", audiGear=" + audiGear +
                ", consumptionA5=" + consumptionA7 +
                '}';
    }
}
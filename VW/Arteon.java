package VW;

import Car.Car;

public class Arteon extends Car implements IVW {

    String carColour;
    public String volkswagenCarName; //used to name the phone model
    public String volkswagenChasisNo;
    public int volkswagenGear;
    private String fuelType;

    private int consumptionArteon;


    private static int getVolkswagenFuelTankSize() {
        return volkswagenFuelTankSize;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public Arteon(String carColour, String volkswagenCarName, String volkswagenChasisNo, int volkswagenGear) {
        super(7, 1, 8, 0.3);
        this.carColour = carColour;
        this.volkswagenCarName = volkswagenCarName;
        this.volkswagenChasisNo = volkswagenChasisNo;
        this.volkswagenGear = volkswagenGear;


    }

    @Override
    public void reFillTank(double availableFuel) {
        if (this.availableFuel + availableFuel > volkswagenFuelTankSize + 8) {
            this.availableFuel = volkswagenFuelTankSize + 8;
            System.out.println("can't fill with more than maximum A7 tank capacity");
            System.out.println("tank full");
        } else {
            this.availableFuel = availableFuel + this.availableFuel;
            System.out.println("available fuel in car after refill is: " + this.availableFuel);
        }

    }


    @Override
    public String toString() {
        return "A7{" +
                "carColour='" + carColour + '\'' +
                ", volkswagenCarName='" + volkswagenCarName + '\'' +
                ", volkswagenChasisNo='" + volkswagenChasisNo + '\'' +
                ", volkswagenGear=" + volkswagenGear +
                ", consumptionA5=" + consumptionArteon +
                '}';
    }
}


